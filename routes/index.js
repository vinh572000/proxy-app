var express = require('express');
var axios = require('axios');
var router = express.Router();
var sql = require('mssql');

const config = {
  user: 'sa',
  password: '12345a@',
  server: '192.168.1.5', // or IP address
  database: 'AutoInsertTicket',
  trustServerCertificate: true,
  options: {
    port: 1433,
    encrypt: true, // Use this if you're on Windows Azure
  },
};

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});
//set proxy

//list tickets 
router.post('/getallticket', async (req, res, next) => {
  try {
    const requestBody = req.body;
    //console.log('token', requestBody);
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded', // Set the content type based on your API requirements
    };

    // Make an API call to a sample JSONPlaceholder API
    const apiResponse = await axios.post('https://request.base.vn/extapi/v1/request/list', requestBody, { headers });

    // Extract data from the API response
    const todoData = apiResponse.data;
    console.log('data', todoData);

    // Send the data as a response
    res.json({ todoData });
  } catch (error) {
    // Handle errors
    console.error('Error making API call:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

router.post('/insertdb', async (req, res, next) => {
  try {
    // Create a new connection pool
    // const pool = await sql.connect(config);
    const pool = await sql.connect(config)
      .then(() => {
        console.log('Connected to SQL Server');
      })
      .catch((err) => {
        console.error('Error connecting to SQL Server:', err);
      })
    //return;
    const tickets = req.body.tickets;
    const transaction = new sql.Transaction(pool);
    await transaction.begin();

    const request = new sql.Request();

    try {
      let currentDate = new Date();
      let prevDate = new Date(currentDate.setMonth(currentDate.getMonth() - 6));
      for (const record of tickets) {
        let lastUpdate = new Date(record.last_update * 1000);
        if (lastUpdate > prevDate) {
          const checkExists = await request.query(`SELECT * FROM dbo.tickets WHERE Id_ticket = ${record.id}`);
          if (checkExists.recordset.length > 0) {
            await request.query(
              `UPDATE dbo.tickets SET name_ticket = N'${record.name}', content_ticket =  N'${record.content}' ,file_names=  N'${record.file_names}', rejecters=  '${record.rejecters} '
              ,approvals= ' ${record.approvals}' ,username = N'${record.username}' ,since=  '${record.since}' ,last_update=  '${record.last_update}', approval_final='${record.approval_final}'
              ,full_name_created=N'${record.full_name_created}',owners='${record.owners}'
               WHERE Id_ticket =  '${record.id}'`);
          } else {
            await request.query(`INSERT INTO dbo.tickets (Id_ticket, name_ticket,content_ticket,file_names,rejecters,approvals,username,since,last_update,approval_final,full_name_created,approval_next,owners) 
              VALUES ('${record.id}',N'${record.name}',N'${record.content}',N'${record.file_names}','${record.rejecters}','${record.approvals}',N'${record.username}','${record.since}','${record.last_update}','${record.approval_final}',N'${record.full_name_created}','${record.approval_next}','${record.owners}')`);
          }
        } else {
          continue;
        }
      }
      // Commit the transaction if all inserts succeed
      await transaction.commit();
      // Respond with success
      res.status(200).json({ success: true, message: 'Records inserted successfully.' });
    } catch (error) {
      // Rollback the transaction if any insert fails
      console.log('Rollback the transaction')
      await transaction.rollback();
      throw err;
    }

  } catch (error) {
    console.error('Error making API call:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
})

//list groups

router.post('/getallgroup', async (req, res, next) => {
  try {
    const requestBody = req.body;
    console.log('token', requestBody);
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded', // Set the content type based on your API requirements
    };

    // Make an API call to a sample JSONPlaceholder API
    const apiResponse = await axios.post('https://request.base.vn/extapi/v1/group/list', requestBody, { headers });

    // Extract data from the API response
    const todoData = apiResponse.data;
    //console.log('data', todoData);

    // Send the data as a response
    res.json({ todoData });
  } catch (error) {
    // Handle errors
    console.error('Error making API call:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
})


router.post('/insertGroup', async (req, res, next) => {
  try {
    // Create a new connection pool
    // const pool = await sql.connect(config);
    const pool = await sql.connect(config)
      .then(() => {
        console.log('Connected to SQL Server');
      })
      .catch((err) => {
        console.error('Error connecting to SQL Server:', err);
      })
    //return;
    const groups = req.body.groups;
    // console.log(groups);
    // return;
    const transaction = new sql.Transaction(pool);
    await transaction.begin();

    const request = new sql.Request();

    try {
      for (const record of groups) {
        const checkExists = await request.query(`SELECT * FROM dbo.groups WHERE id = ${record.id}`);
        if (checkExists.recordset.length > 0) {
          // console.log('1');
          // return;
          await request.query(
            `UPDATE dbo.groups SET gid = '${record.gid}', username =  '${record.username}', category=  N'${record.category} '
                 ,content_group= N' ${record.content}' ,name_group = N'${record.name}' ,permission_edit_followers=  '${record.permission_edit_followers}' ,
                 fix_followers=  '${record.fix_followers}', add_task_follower='${record.add_task_follower}'
                   ,approved_block_edit_comment='${record.approved_block_edit_comment}'
                   ,allow_followers_export_action='${record.allow_followers_export_action}'
                   ,allow_approvers_export_action='${record.allow_approvers_export_action}'
                   ,allow_return_request='${record.allow_return_request}'
                   ,allow_transfer_approver='${record.allow_transfer_approver}'
                   ,notis_sequential='${record.notis_sequential}'
                   ,email='${record.email}'
                    WHERE id =  '${record.id}'`);
        } else {
          //console.log('2');
          //return;
          await request.query(`INSERT INTO dbo.groups (id, gid,username,category,content_group,name_group,permission_edit_followers,
            fix_followers,add_task_follower,approved_block_edit_comment,allow_approvers_export_action,allow_followers_export_action,
            allow_return_request,allow_transfer_approver,notis_sequential,email) 
                 VALUES ('${record.id}','${record.gid}','${record.username}',N'${record.category}',N'${record.content}',
                 N'${record.name}','${record.permission_edit_followers}','${record.fix_followers}','${record.add_task_follower}',
                 '${record.approved_block_edit_comment}','${record.allow_approvers_export_action}','${record.allow_followers_export_action}',
                 '${record.allow_return_request}','${record.allow_transfer_approver}','${record.notis_sequential}','${record.email}')`);
        }
      }
      // Commit the transaction if all inserts succeed
      await transaction.commit();
      // Respond with success
      res.status(200).json({ success: true, message: 'Records inserted successfully.' });
    } catch (error) {
      // Rollback the transaction if any insert fails
      console.log('Rollback the transaction')
      await transaction.rollback();
      throw error;
    }

  } catch (error) {
    console.error('Error making API call:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
})



module.exports = router;
